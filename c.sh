#!/bin/bash
aria2c -j5 -i list.txt -c --save-session out.txt
has_error=`wc -l < out.txt`

while [ $has_error -gt 0 ]
do
  echo "still has $has_error errors, rerun aria2 to download ..."
  aria2c -j5 -i list.txt -c --seed-time=0 --save-session out.txt
  has_error=`wc -l < out.txt`
  sleep 10
done

### PS: one line solution, just loop 1000 times
###         seq 1000 | parallel -j1 aria2c -i list.txt -c